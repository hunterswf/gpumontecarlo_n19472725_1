package monteCarlo;

public class AntiTheticVectorGenerator implements RandomVectorGenerator{
	
	private RandomVectorGenerator rvg;
	double[] lastVector;

	public AntiTheticVectorGenerator(RandomVectorGenerator rvg){
		this.rvg = rvg;
	}

	@Override
	public double[] getVector() {
		if ( lastVector == null ){
			lastVector = rvg.getVector();
			return lastVector;
		} else {
			double[] tmp = new double[lastVector.length];
			for (int i = 0; i < tmp.length; ++i){ tmp[i] = -1*lastVector[i];}
			lastVector = null;
			return tmp;
		}
	}
	
}

