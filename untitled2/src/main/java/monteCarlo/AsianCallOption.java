package monteCarlo;

import java.util.List;

import org.joda.time.DateTime;

public class AsianCallOption implements PayOut {
	private double K;//strike price
	private double r;//rate per day
	private DateTime startDate;
	private DateTime endDate;
	
	public AsianCallOption(double K, double r, DateTime startDate, DateTime endDate){
		this.K = K;
		this.r = r;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	@Override
	public double getPayout(StockPath path) {
		// TODO Auto-generated method stub
		List<Pair<DateTime, Double>> prices = path.getPrices();
		double sum = 0;
		for(Pair price : prices){	
			sum += price.getValue();
		}
		double average = sum/prices.size();
		double t = (endDate.getMillis() - startDate.getMillis())/86400000;
		return Math.exp(-1*r*t)*Math.max(0, average - K);
	}

}
