package monteCarlo;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

public class GBMRandomPathGenerator implements StockPath {
	
	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;
	
	public GBMRandomPathGenerator(double rate, int N,
			double sigma, double S0,
			DateTime startDate, DateTime endDate,
			RandomVectorGenerator rvg){
		this.startDate = startDate;
		this.endDate = endDate;
		this.rate = rate;
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;
	}

	@Override
	public List<Pair<DateTime, Double>> getPrices() {
		double[] n = rvg.getVector();
		DateTime current = new DateTime(startDate.getMillis());
		long delta = (endDate.getMillis() - startDate.getMillis())/N;
		double deltaPerDay = (double) delta/86400000;
		double deltaPerDayRoot = Math.sqrt(deltaPerDay);
		double c = rate-sigma*sigma/2;
		
		List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime, Double>(current, S0));
		if(N == 252){//Simulate on daily base
			for ( int i=1; i < N; i++){
				current = current.plusMillis((int) delta);
				path.add(new Pair<DateTime, Double>(current, 
				         path.get(path.size()-1).getValue()*Math.exp(c+sigma * n[i-1])));
			}
			return path;
		}else{//Simulate on other base
			for ( int i= 1; i < N; i++){
				current = current.plusMillis((int) delta);
				path.add(new Pair<DateTime, Double>(current, 
				         path.get(path.size()-1).getValue()*Math.exp(c*deltaPerDay +sigma * n[i-1]*deltaPerDayRoot)));
			} 
			return path;	
		}
	}
}
