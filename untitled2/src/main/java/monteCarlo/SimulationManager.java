package monteCarlo;

public class SimulationManager {
	private StockPath sp;
	private PayOut po;
	
	public SimulationManager(StockPath sp, PayOut po){
		this.sp = sp;
		this.po = po;
	}
	
	public double getPrice(){
		int count = 0;
		
		DataTrack sc = new DataTrack();
		
		double payout = 0;
		
			do{
				payout = po.getPayout(sp);
				sc.addStats(payout);
				count++;
				double sigmasq = sc.getSigmasq();
				System.out.println(payout);
				System.out.println(sigmasq);
				System.out.println(3.0625*sigmasq/0.0001);//3.0625=1.75^2 which is the threshold for 96%
				System.out.println(count);
				System.out.println("-------------------------------------------");
				count++;
				if(sigmasq !=0 && (double)3.0625*sigmasq/0.0001 < count) break;
			
			
			}while(true);	
		
		System.out.println(sc.getMean());
		System.out.println(count);
		return sc.mean;
		
	}
}
