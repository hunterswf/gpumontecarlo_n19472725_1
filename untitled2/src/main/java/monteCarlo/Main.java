package monteCarlo;

import org.joda.time.DateTime;

public class Main {
	public static void main(String[] args){
		
		int N = 1000;
		int batchNum = 2000000;
		double r = 0.0001;
		double sigma = 0.01;
		
		DateTime startDate = new DateTime();
		DateTime endDate = startDate.plusDays(252);
		
		GPUNormalGenerator gpu = new GPUNormalGenerator(N, batchNum);
		AntiTheticVectorGenerator avg = new AntiTheticVectorGenerator(gpu);
		GBMRandomPathGenerator GBM_sp = new GBMRandomPathGenerator(r, N, sigma, 152.35, startDate, endDate, avg);
		
		EuropeanCallOption ecp = new EuropeanCallOption(165, r, startDate, endDate);
		AsianCallOption acp = new AsianCallOption(164, r, startDate, endDate);
		
		SimulationManager sm1 = new SimulationManager(GBM_sp, ecp);
		double price1 = sm1.getPrice();
		SimulationManager sm2 = new SimulationManager(GBM_sp, acp);
		double price2 = sm2.getPrice();
		
		System.out.println("The European Option price is " + price1 + '.');
		System.out.println("The Asian Option price is " + price2 + '.');
	}

}
