package monteCarlo;

import java.util.Random;

public class NormalRandomNumberGenerator implements RandomVectorGenerator {
	private int N;
    private double sigma;
    private double miu;
	
	public NormalRandomNumberGenerator(int N, double miu, double sigma){
		this.N = N;
		this.sigma = sigma;
		this.miu = miu;
	}

	@Override
	public double[] getVector() {
		Random r = new Random();
		double[] vector = new double[N];
		for ( int i = 0; i < vector.length; ++i){
			vector[i] = r.nextGaussian()*sigma+miu;
		}
		return vector;
	}

}
