package monteCarlo;

public interface PayOut {
	
	public double getPayout(StockPath path);

}
