package monteCarlo;

public class DataTrack {
	double sum;
	double mean;
	double sumOfSquare;
	double sigmasq;
	int N;
	
	public DataTrack(){
		this.sum = 0.0;
		this.mean = 0.0;
		this.sumOfSquare = 0.0;
		this.sigmasq = 0.0;
		this.N = 0;
	}
	
	public double getMean(){
		return mean;
	}
	
	public void addStats(double x){
		N++;
		sum = sum + x;
		mean = sum/N;
		sumOfSquare = sumOfSquare + x*x;
		sigmasq = sumOfSquare/N - mean * mean;
	}
	
	public double getSigmasq(){
		return sigmasq;
	}

}
