package monteCarlo;

/**
 * Created by WeifanSun on 12/12/14.
 */
import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import java.util.Random;

import static org.bridj.Pointer.allocateFloats;

public class GPUNormalGenerator implements RandomVectorGenerator {
    private int N;         // length of random vector required
    private int batchNum;  // batch number required
    private double[] normalVector;  // store the normal random vector stored in GPU to generate Gaussian
    private int rerun;    // tracks whether we need to regenerate the normal random variable or not

    // GPU Generator constructor
    public GPUNormalGenerator(int N, int batchNum){
        this.N = N;
        this.batchNum = batchNum;
        normalVector = getGPUGaussian(this.batchNum);
        rerun = 0;
    }

    // This function aims to generate normal random variable using the best device chosen by kernal

    public double[] getGPUGaussian(int batchNum){
        rerun = 0;
        CLPlatform clp = JavaCL.listPlatforms()[0];  // Creating the platform which is out computer.
        CLDevice cld = clp.listAllDevices(true)[0]; // Getting the all devices
        CLContext clc = JavaCL.createContext(null, cld);
        CLQueue clq = clc.createDefaultQueue();
        // Read the program sources and compile them :
        String src = "__kernel void fill_in_values(__global const float* a, __global const float* b, __global float* out1, __global float* out2, float pi, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out1[i] = sqrt(-2*log(a[i]))*cos(2*pi*b[i]);\n" +
                "    out2[i] = sqrt(-2*log(a[i]))*sin(2*pi*b[i]);\n" +
                "}";
        CLProgram program = clc.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("fill_in_values");

        final int n = batchNum;
        final Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);
        // store the uniform vector in order for GPU to generate Gaussian
        Random r = new Random();
        double[] uniformVector = new double[2*batchNum];
        for ( int i = 0; i < uniformVector.length; ++i){
            uniformVector[i] = r.nextDouble();
        }

        // Generate uniform sequence and assign to aPtr and bPtr
        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float) uniformVector[2*i]);
            bPtr.set(i, (float) uniformVector[2*i+1]);
            //System.out.println(uniformVector[2*i]);
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = clc.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = clc.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float>
                out1 = clc.createFloatBuffer(CLMem.Usage.Output, n),
                out2 = clc.createFloatBuffer(CLMem.Usage.Output, n);

        kernel.setArgs(a, b, out1, out2, (float) Math.PI, batchNum);
        CLEvent event = kernel.enqueueNDRange(clq, new int[]{n}, new int[]{128});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {

            }
        });
        final Pointer<Float> c1Ptr = out1.read(clq,event);
        final Pointer<Float> c2Ptr = out2.read(clq,event);

        double[] normalVector = new double[2*batchNum];
        for(int i = 0; i < batchNum; i++){
            normalVector[2*i] = (double) c1Ptr.get(i);
            normalVector[2*i+1] = (double) c2Ptr.get(i);
        }
        return normalVector;
    }

    @Override
    public double[] getVector() {
        double[] vector = new double[N];
        for(int i = 0; i < N; i++){
            // if the rerun reaches the generated amount of normal random number
            // we regenerate a batch of random number again
            if(rerun == 2*batchNum - 1) {
                normalVector = getGPUGaussian(this.batchNum);
            }
            // we assign the generated normal to the vector needed for the simulation
            vector[i] = normalVector[rerun];
            rerun++;
        }
        return vector;
    }
}


