Please follow the following steps:

Step 1. Open the IntelliJ;
Step 2. Clone all files from git clone https://hunterswf@bitbucket.org/hunterswf/gpumontecarlo_n19472725_1.git to a local directory;
Step 3. Create a new Maven project based on the copied files in GPUMonteCarlo_N19472725_1;
Step 4. Open the “Main.java", and run it. The result is shown in the console.

RESULTS_GPU: shows the result for Europe and Asian options with number of simulations.

Camel Java Router Project
=========================

To build this project use

    mvn install

To run this project from within Maven use

    mvn exec:java

For more help see the Apache Camel documentation

    http://camel.apache.org/

